FROM nextflow/nextflow

LABEL version="0.1" \
      description="Oxford MMM CI helper image" \
      maintainer="crookscs.it@ndm.ox.ac.uk" \
      dockerhub="oxfordmmm/mmm-ci:v0.1"

RUN apk update && apk add python3
